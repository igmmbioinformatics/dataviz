# dataviz

Data visualisation resources


Good starting point

Points of View columns on data visualization published in Nature Methods

http://blogs.nature.com/methagora/2013/07/data-visualization-points-of-view.html

## People

A list of people  who are researching data viz in Bioinformatics

|Name|Location|Title|Twitter|
|-----|-------|-----|-------|
|[Benjamin Bach](https://www.designinformatics.org/person/benjaminbach/)|Edinburgh|Lecturer in Design Informatics and Visualization at the University of Edinburgh.|
|[Martin Krzywinski](http://mkweb.bcgsc.ca/)|BC genomics Canada|
|[James Proctor](https://www.lifesci.dundee.ac.uk/people/jim-procter)|Dundee|
|[Professor Sean O'Donoghue](https://www.garvan.org.au/people/seaodo)|Garvan Institute|
|Jan Aerts|||@jandot|

## Papers

|Title|PMID|Reference|Abstract|
|-----|----|---------|--------|
|[Visualization of Biomedical Data](https://www.annualreviews.org/doi/abs/10.1146/annurev-biodatasci-080917-013424)|||The rapid increase in volume and complexity of biomedical data requires changes in research, communication, and clinical practices. This includes learning how to effectively integrate automated analysis with high–data density visualizations that clearly express complex phenomena. In this review, we summarize key principles and resources from data visualization research that help address this difficult challenge. We then survey how visualization is being used in a selection of emerging biomedical research areas, including three-dimensional genomics, single-cell RNA sequencing (RNA-seq), the protein structure universe, phosphoproteomics, augmented reality–assisted surgery, and metagenomics. While specific research areas need highly tailored visualizations, there are common challenges that can be addressed with general methods and strategies. Also common, however, are poor visualization practices. We outline ongoing initiatives aimed at improving visualization practices in biomedical research via better tools, peer-to-peer learning, and interdisciplinary collaboration with computer scientists, science communicators, and graphic designers. These changes are revolutionizing how we see and think about our data.|
|[Beyond Bar and Line Graphs: Time for a New Data Presentation Paradigm](https://journals.plos.org/plosbiology/article?id=10.1371/journal.pbio.1002128)|||Figures in scientific publications are critically important because they often show the data supporting key findings. Our systematic review of research articles published in top physiology journals (n = 703) suggests that, as scientists, we urgently need to change our practices for presenting continuous data in small sample size studies. Papers rarely included scatterplots, box plots, and histograms that allow readers to critically evaluate continuous data. Most papers presented continuous data in bar and line graphs. This is problematic, as many different data distributions can lead to the same bar or line graph. The full data may suggest different conclusions from the summary statistics. We recommend training investigators in data presentation, encouraging a more complete presentation of data, and changing journal editorial policies. Investigators can quickly make univariate scatterplots for small sample size studies using our Excel templates.|
|[Tasks, Techniques, and Tools for Genomic Data Visualization](https://onlinelibrary.wiley.com/doi/full/10.1111/cgf.13727)|||Genomic data visualization is essential for interpretation and hypothesis generation as well as a valuable aid in communicating discoveries. Visual tools bridge the gap between algorithmic approaches and the cognitive skills of investigators. Addressing this need has become crucial in genomics, as biomedical research is increasingly data-driven and many studies lack well-defined hypotheses. A key challenge in data-driven research is to discover unexpected patterns and to formulate hypotheses in an unbiased manner in vast amounts of genomic and other associated data. Over the past two decades, this has driven the development of numerous data visualization techniques and tools for visualizing genomic data. Based on a comprehensive literature survey, we propose taxonomies for data, visualization, and tasks involved in genomic data visualization. Furthermore, we provide a comprehensive review of published genomic visualization tools in the context of the proposed taxonomies.|
## Meetings

|Meeting|Description|
|--------|----------|
|[vizbi](https://vizbi.org)|The VIZBI initiative is an international conference series bringing together researchers developing and using computational visualisation to address a broad range of biological research areas; the conference also attracts participation from medical illustrators, graphic designers, and graphic artists. VIZBI is held annually, alternating between Europe and the USA.|



## Edinburgh groups

https://www.meetup.com/meetup-group-vBHbCmgh/


## R


packages

|Package|Description|
|-------|-----------|
|ggplot2||
|[plotly](https://plotly.com/r/)||
|scatterD3||
|d3heatmap||
|[crosstalk](https://rstudio.github.io/crosstalk/)||

* Data visualisation in R chapter from R for data science [here].(https://r4ds.had.co.nz/data-visualisation.html)

## Useful Links

* https://en.wikipedia.org/wiki/Exploratory_data_analysis
* 